import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BeauticianPageComponent } from './beautician-page.component';

describe('BeauticianPageComponent', () => {
  let component: BeauticianPageComponent;
  let fixture: ComponentFixture<BeauticianPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeauticianPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeauticianPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
