import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AdminLoginComponent } from './screens/admin-login/admin-login.component';
import { FooterComponent } from './shared/footer/footer.component';
import { HeaderComponent } from './shared/header/header.component';
import { SidebarComponent } from './shared/sidebar/sidebar.component';
import { DashboardComponent } from './screens/dashboard/dashboard.component';
import { TablesComponent } from './screens/tables/tables.component';
import { BeauticianPageComponent } from './screens/beautician-page/beautician-page.component';
import { PaymentComponent } from './screens/payment/payment.component';
import { CouponsComponent } from './screens/coupons/coupons.component';
import { ServicesInfoComponent } from './screens/services-info/services-info.component';


@NgModule({
  declarations: [
    AppComponent,
    AdminLoginComponent,
    FooterComponent,
    HeaderComponent,
    SidebarComponent,
    DashboardComponent,
    TablesComponent,
    BeauticianPageComponent,
    PaymentComponent,
    CouponsComponent,
    ServicesInfoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
   
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
